# RC_package

A complete package of RIAM-Compact.


## REVISION HISTORY

---
- 2017-03-30  Version 1.0.9
  - TextParser-1.8.2
  - PMlib-5.6.1
  - CPMlib-2.4.0
  - CDMlib-1.0.9
  - RIAM-Compact-0.9.6

  
---
- 2017-02-25  Version 1.0.8
  - PMlib-5.5.9
  - CPMlib-2.3.4
  - CDMlib-1.0.7
  - RIAM-Compact-0.8.12

  
---
- 2017-02-15  Version 1.0.7
  - CPMlib-2.3.3
  

---
- 2017-02-15  Version 1.0.6
  - TextParser-1.8.1
  - PMlib-5.5.7
  - CPMlib-2.3.2
  - CDMlib-1.0.6
  - RIAM-Compact-0.8.1
  - Tested.

  |Environment|Serial|MPI |
  |:--|:--:|:--:|
  |Intel / Intel 17.0.1 ||ok|
  |Intel / GNU 6.2.0    ||ok|
  |Fujitsu / fx10       ||ok|
  |Fujitsu / fx100      ||ok|
  |Fujitsu / K          |||


---
- 2017-02-15. Verion 1.0.5
  - update to CPMlib-2.2.3


---
- 2017-02-13. Vesion 1.0.4
  - update to RIAM-Compact-0.7.6
  - update install.sh


---
- 2017-02-12. Version 1.0.3
  - update to RIAM-Compact-0.7.5


---
- 2017-02-12. Version 1.0.2
  - update fx10 compiler options
  - modify `install.sh`.


---
- 2017-02-12. Version 1.0.1
  - modify `install.sh`.


---
- 2017-02-12  Version 1.0.0
  - Cmake version
  - Install shell scripts are integrated into `install.sh`.


---
- 2017-01-27. Verion 0.8.0
  - TextParser-1.7.3
  - RIAM-Compact-0.7.2
  - Modify install_*.sh
  - add ChangeLog.md


---
- 2017-01-26. Version 0.7.1
  - modify compiler options for fx10


---
- 2017-01-23. Version 0.7.0
  - Cmake version
 
 
