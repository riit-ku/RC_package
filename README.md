# RC_package

Release package of RIAM-Compact and related libraries.

Riam Compact is the acronym standing for Research Institute for Applied Mechanics, Kyushu University, Computational Prediction of Airflow over Complex Terrain, and is a numerical-CFD-model/software-package which has an object domain from several m to several km, and can predict the airflow and the gas diffusion over complex terrain with high precision.


## Copyright
- Copyright (c) 2015-2017 Research Institute for Applied Mechanics(RIAM), Kyushu University. All rights reserved.
- Copyright (c) 2016-2017 Research Institute for Information Technology(RIIT), Kyushu University. All rights reserved.
- Copyright (c) 2015-2016 Advanced Institute for Computational Science(AICS), RIKEN. All rights reserved.


## Prerequisite

- Cmake
- MPI library
- TextParser (included in this package)
- PMlib (included in this package)
- CPMlib (included in this package)
- CDMlib (included in this package)


## Install
Type install shell script on command line with options .

~~~
$ export CC=... CXX=... F90=... FC=...
$ ./install.sh <intel|fx10|K> <INST_DIR> {serial|mpi} {double|float}
~~~


## Supported platforms and compilers

* Linux Intel arch., Intel/GNU compilers.
* Mac OSX, Intel/GNU compilers.
* Sparc fx arch. and Fujitsu compiler.



## Contributors

- Takanori Uchida
- Kenji Ono *keno@cc.kyushu-u.ac.jp*


