#! /bin/sh
#
##############################################################################
#
# RIAM-Compact CPMlib versiopn
#
# Copyright (C) 2015-2017 Research Institute for Applied Mechanics(RIAM)
#                       / Research Institute for Information Technology(RIIT), Kyushu University.
# All rights reserved.
#
# Copyright (c) 2015-2016 Advanced Institute for Computational Science, RIKEN.
# All rights reserved.
#

#######################################
#
# Library version
#

export TP_LIB=TextParser-1.8.2
export PM_LIB=PMlib-5.6.1
export CPM_LIB=CPMlib-2.4.0
export CDM_LIB=CDMlib-1.0.9
export RIAMC=RIAM-Compact-0.9.6



#######################################
#
# Usage
#
# $ ./install.sh <intel|fx10|K> <INST_DIR> {serial|mpi} {double|float}
#
#######################################


# Target machine
#
target_arch=$1

if [ "${target_arch}" = "intel" ]; then
  echo "Target arch       : intel"
elif [ "${target_arch}" = "fx10" ]; then
  echo "Target arch       : fx10"
  sparcv9="yes"
  toolchain_file="../cmake/Toolchain_fx10.cmake"
elif [ "${target_arch}" = "K" ]; then
  echo "Target arch       : K Computer"
  sparcv9="yes"
  toolchain_file="../cmake/Toolchain_K.cmake"
else
  echo "Target arch       : not supported -- terminate install process"
  exit
fi


# Install directory
#
target_dir=$2

export INST_DIR=${target_dir}
echo "Install directory : ${INST_DIR}"


# Parallelism
#
parallel_mode=$3

if [ "${parallel_mode}" = "mpi" ]; then
  echo "Paralle mode.     : ${parallel_mode}"
elif [ "${parallel_mode}" = "serial" ]; then
  echo "Paralle mode.     : ${parallel_mode}"
else
  echo "Paralle mode.     : Invalid -- terminate install process"
  exit
fi


# Floating point Precision
#
fp_mode=$4

if [ "${fp_mode}" = "double" ]; then
  export PRCSN=double
elif [ "${fp_mode}" = "float" ]; then
  export PRCSN=float
else
  echo "Invalid argument for floating point  -- terminate install process"
  exit
fi

echo "Precision         : ${fp_mode}"
echo " "


#######################################


# TextParser
#
echo
echo -----------------------------
echo Install TextParser
echo
if [ ! -d ${TP_LIB} ]; then
  tar xvzf ${TP_LIB}.tar.gz
  mkdir ${TP_LIB}/build
  cd ${TP_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -Dwith_MPI=yes ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -Dwith_MPI=no ..
    fi

  elif [ "${sparcv9}" = "yes" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=yes ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/TextParser \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=no ..
    fi
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${TP_LIB} is already exist. Skip compiling."
fi



# PMlib
#
echo
echo -----------------------------
echo Install PMlib
echo
if [ ! -d ${PM_LIB} ]; then
  tar xvzf ${PM_LIB}.tar.gz
  mkdir ${PM_LIB}/build
  cd ${PM_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -Denable_OPENMP=no \
            -Dwith_MPI=yes \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=no \
            -Dwith_OTF=no ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -Denable_OPENMP=no \
            -Dwith_MPI=no \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=no \
            -Dwith_OTF=no ..
    fi

  elif [ "${sparcv9}" = "yes" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Denable_OPENMP=no \
            -Dwith_MPI=yes \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=no \
            -Dwith_OTF=no ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/PMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Denable_OPENMP=no \
            -Dwith_MPI=no \
            -Denable_Fortran=no \
            -Dwith_example=no \
            -Dwith_PAPI=no \
            -Dwith_OTF=no ..
    fi
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${PM_LIB} is already exist. Skip compiling."
fi



# CPMlib
#
echo
echo -----------------------------
echo Install CPMlib
echo
if [ ! -d ${CPM_LIB} ]; then
  tar xvzf ${CPM_LIB}.tar.gz
  mkdir ${CPM_LIB}/build
  cd ${CPM_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CPMlib \
            -Dwith_MPI=yes \
            -Dreal_type=${PRCSN} \
            -Denable_LMR=no \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/CPMlib \
            -Dwith_MPI=no \
            -Dreal_type=${PRCSN} \
            -Denable_LMR=no \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..
    fi

  elif [ "${sparcv9}" = "yes" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CPMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=yes \
            -Dreal_type=${PRCSN} \
            -Denable_LMR=no \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/CPMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=no \
            -Dreal_type=${PRCSN} \
            -Denable_LMR=no \
            -Dwith_example=no \
            -Dwith_TP=${INST_DIR}/TextParser ..
    fi
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${CPM_LIB} is already exist. Skip compiling."
fi



# CDMlib
#
echo
echo -----------------------------
echo CDMlib
echo
if [ ! -d ${CDM_LIB} ]; then
  tar xvzf ${CDM_LIB}.tar.gz
  mkdir ${CDM_LIB}/build
  cd ${CDM_LIB}/build

  if [ "${target_arch}" = "intel" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CDMlib \
            -Dwith_MPI=yes \
            -Dwith_example=no \
            -Dwith_util=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_NetCDF=no \
            -Denable_BUFFER_SIZE=no ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/CDMlib \
            -Dwith_MPI=no \
            -Dwith_example=no \
            -Dwith_util=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_NetCDF=no \
            -Denable_BUFFER_SIZE=no ..
    fi

  elif [ "${sparcv9}" = "yes" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/CDMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=yes \
            -Dwith_example=no \
            -Dwith_util=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_NetCDF=no \
            -Denable_BUFFER_SIZE=no ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/CDMlib \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dwith_MPI=no \
            -Dwith_example=no \
            -Dwith_util=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_NetCDF=no \
            -Denable_BUFFER_SIZE=no ..
    fi
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${CDM_LIB} is already exist. Skip compiling."
fi




# RIAMC
#
echo
echo -----------------------------
echo Install RIAM-COMPACT
echo
if [ ! -d ${RIAMC} ]; then
  tar xvzf ${RIAMC}.tar.gz
  mkdir ${RIAMC}/build
  cd ${RIAMC}/build

  if [ "${target_arch}" = "intel" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/RIAMC \
            -Dreal_type=${PRCSN} \
            -Denable_OPENMP=yes \
            -Dwith_MPI=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PM=${INST_DIR}/PMlib \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_CDM=${INST_DIR}/CDMlib ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/RIAMC \
            -Dreal_type=${PRCSN} \
            -Denable_OPENMP=yes \
            -Dwith_MPI=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PM=${INST_DIR}/PMlib \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_CDM=${INST_DIR}/CDMlib ..
    fi

  elif [ "${sparcv9}" = "yes" ]; then
    if [ "${parallel_mode}" = "mpi" ]; then
      cmake -DINSTALL_DIR=${INST_DIR}/RIAMC \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dreal_type=${PRCSN} \
            -Denable_OPENMP=yes \
            -Dwith_MPI=yes \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PM=${INST_DIR}/PMlib \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_CDM=${INST_DIR}/CDMlib ..
    else
      cmake -DINSTALL_DIR=${INST_DIR}/RIAMC \
            -DCMAKE_TOOLCHAIN_FILE="${toolchain_file}" \
            -Dreal_type=${PRCSN} \
            -Denable_OPENMP=yes \
            -Dwith_MPI=no \
            -Dwith_TP=${INST_DIR}/TextParser \
            -Dwith_PM=${INST_DIR}/PMlib \
            -Dwith_CPM=${INST_DIR}/CPMlib \
            -Dwith_CDM=${INST_DIR}/CDMlib ..
    fi
  fi

  make
  if [ $? -ne 0 ]; then
    echo "make error!"
    exit
  fi
  make install
  cd ../..
else
  echo "${RIAMC} is already exist. Skip compiling."
fi
